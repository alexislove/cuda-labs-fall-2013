/*!
 *\file ReductionKernel.cuh
 *\brief This file contains implementation of a simple reduction kernel
 */

#ifndef REDUCTIONKERNEL_CUH
  #define REDUCTIONKERNEL_CUH
  
  // Wrapper macro for fast integer multiplication
  #if !defined(IMUL)
    #define IMUL(x, y) (__mul24((x), (y)))
  #endif // IMUL

  /*!
   *\brief Kernel for sum reduction computation
   *\param array     Source array
   *\param size      Size of the source array
   *\param output    Output sum per each block
   */
  __global__ void cuReductionKernel(const float* array,
                                    int          size,
                                    float* const output)
  {
    extern __shared__ float sCache[]; 

    const int tx = threadIdx.x;
    const int bx = blockIdx.x; 
    const int bw = blockDim.x;

    const int loadIndex = bx * bw + tx;

    sCache[tx] = (loadIndex < size) ? array[loadIndex] : 0.f;

    __syncthreads();

    for (unsigned k = bw / 2; k > 0; k >>= 1) 
    {
      if (tx < k)
      {
        sCache[tx] += sCache[tx + k];
      }

      __syncthreads();
    }

    
    if (tx == 0)
    {
      float v = sCache[0];
      output[blockIdx.x] = v;
    }
  }


#endif // REDUCTIONKERNEL_CUH