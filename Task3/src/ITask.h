/*!
 *\file ITask.h
 *\brief This file contains task solution interface definition
 */

#ifndef ITASK_H
  #define ITASK_H

  #include <string>

  /*!
   *\class ITask
   *\brief Task interface definition
   */
  struct ITask
  {
    /*!
     *\brief Destructor
     */
    virtual ~ITask() = 0
    {

    }

    /*!
     *\brief Run task with the specified input and output files
     *\param inputFileName  Name of the input file
     *\param outputFileName Name of the output file
     *\return True, in case task solution succeeded
     */
    virtual bool solve(const std::string& inputFileName, const std::string& outputFileName) = 0;
  };

#endif // ITASK_H