/*!
 *\file TaskSerializable.cpp
 *\brief This file contains input/output serialization class implementation
 */

#include <algorithm>
#include <cassert>
#include <iomanip>
#include <string>
#include <sstream>
#include <vector>

#include "TaskSerializable.h"

#define ARRAY_DIM_LIMIT 1048576

namespace 
{
  bool readArray(std::istream& input, float* array, int size)
  {
    using namespace std;
    
    string currentLine;
    while (currentLine.empty())
    {
      if (!getline(input, currentLine))
      {
        break;
      }
      if (currentLine.empty())
      {
        continue;
      }
      // Fill matrix row
      istringstream iss(currentLine);

      istream_iterator<string> token(iss);
      istream_iterator<string> end;

      int index = 0;
      for_each(token, end, [&array, &size, &index](const string& s) {
        stringstream ss(s);
        ss >> array[index++];
        assert(index <= size && "Index out of range during reading!");
      });

      // Ill-formed input
      if (index != size) 
      {
        return false;
      }
    }
    
    return true;
  }
}

TaskSerializable::TaskSerializable()
  : mArray(nullptr),
    mSize(0),
    mResult(0.f)
{

}

TaskSerializable::~TaskSerializable()
{
  if (mArray)
  {
    delete[] mArray;
  }
}

bool TaskSerializable::read(std::istream& input)
{
  if (input.eof())
  {
    return false;
  }

  // Reset
  using namespace std;

  // Read array size
  int size = 0;
  input >> size;

  // For convenience of assert description hard-coded constants, 0, 2^20
  assert(size >= 0 && size <= ARRAY_DIM_LIMIT && "Only mentioned dimensions are supported");

  mSize = size;

  // Allocate and read array
  mArray = new float[mSize];
  if (!readArray(input, mArray, mSize))
  {
    return false;
  }

  return true;
}

bool TaskSerializable::write(std::ostream& output) const
{
  output << std::fixed << std::setw(12) << std::setprecision(3) << mResult;

  return true;
}