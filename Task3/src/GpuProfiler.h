/*!
 *\file GpuProfiler.h
 *\brief This file contains CUDA profiler definition
 */

#ifndef GPUPROFILER_H
  #define GPUPROFILER_H

  #include <cuda.h>
  #include <cuda_runtime.h>

  #include <map>
 

  /*!
   *\class GpuProfiler
   *\brief This class represents GPU-related profiler
   */
  class GpuProfiler
  {
  public:
    /*!
     *\brief Constructor
     */
    GpuProfiler()
    {

    }

    /*!
     *\brief Destructor
     */
    ~GpuProfiler()
    {
      
    }

    /*!
     *\brief Initialize profiler
     */
    void initialize()
    {
      cudaEventCreate(&mStart);
      cudaEventCreate(&mStop);
    }

    /*!
     *\brief Destroy profiler
     */
    void release()
    {
      cudaEventDestroy(mStart);
      cudaEventDestroy(mStop);
    }

    /*!
     *\brief Reset profiler instance
     */
    void reset()
    {
      release();
      initialize();
    }

    /*!
     *\brief Start profiling
     */
    void start()
    {
      cudaEventRecord(mStart, 0);
    }

    /*!
     *\brief Stop profiling
     */
    void stop()
    {
      cudaEventRecord(mStop, 0);
    }

    /*!
     *\brief Get time, that GPU routine took part
     *\return Time in milliseconds
     */
    float getMilliseconds()
    {
      float ms = 0.f;
      cudaEventSynchronize(mStop);
      cudaEventElapsedTime(&ms, mStart, mStop);
      return ms;
    }

  private:
    //! Start point
    cudaEvent_t mStart;
    //! End point
    cudaEvent_t mStop;
  };

#endif // CUDAPROFILER_H