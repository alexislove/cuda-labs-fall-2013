/*!
 *\file CpuTask.cpp
 *\brief This file contains CPU task implementation
 */

#include <algorithm>
#include <chrono>
#include <fstream>

#include "CpuProfiler.h"
#include "TaskSerializable.h"

#include "ITask.h"

class CpuTask : public ITask
{
public:
  virtual ~CpuTask() 
  {

  }

  virtual bool solve(const std::string& inputFileName, const std::string& outputFileName); 

};

bool CpuTask::solve(const std::string& inputFileName, const std::string& outputFileName)
{
  CpuProfiler totalProfiler;
  CpuProfiler taskProfiler;

  std::cout << "******************CPU Task***************************" << std::endl;

  return [&outputFileName, &taskProfiler, &totalProfiler] (TaskSerializable* output) -> bool {
    if (!output) {
      return false;
    }
    bool ret = output->write(std::ofstream(outputFileName));
    totalProfiler.stop();

    std::cout << "Total execution time:  " << totalProfiler.getMilliSeconds()   << " ms (" << totalProfiler.getNanoSeconds()            << " ns)" << std::endl;
    std::cout << "Task execution time:   " << taskProfiler.getMilliSeconds()    << " ms (" << taskProfiler.getNanoSeconds()             << " ns)" << std::endl;

    return (delete output, ret);
  } ([&taskProfiler] (TaskSerializable* input) -> TaskSerializable* {
    if (!input) {
      return nullptr;
    }

    taskProfiler.start();

    const float* array = input->getArray();
    const int    size  = input->getArraySize();

    float result = 0.f;

    std::for_each(array, array + size, [&result] (float x) { result += x; });

    TaskSerializable* output = new TaskSerializable;
    output->setResult(result);

    taskProfiler.stop();

    return (delete input, output);
  } ([&inputFileName, &totalProfiler] () -> TaskSerializable* { 
    TaskSerializable* task = new TaskSerializable;
    totalProfiler.start();
    return ((task->read(std::ifstream(inputFileName))) ? task : (delete task, nullptr));
  } ()));
}

ITask* GCreateCPUTask()
{
  return new CpuTask;
}

