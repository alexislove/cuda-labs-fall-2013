/*!
 *\file TaskSerializable.h
 *\brief This file contains input/output serialization class definition
 */

#ifndef TASKSERIALIZABLE_H
  #define TASKSERIALIZABLE_H

  #include <iostream>
  
  /*!
   *\class TaskSerializable
   *\brief This class represents input/output data serialization object
   */
  class TaskSerializable
  {
  public:
    TaskSerializable();

    ~TaskSerializable();

    /*!
     *\brief Read data from the input stream to the output arrays for the matrix and the mask
     *\param input Input stream
     *\param image Image array (not initialized)
     *\param mask  Convolution mask
     *\param rows     Number of rows in RGB matrix
     *\param cols     Number of columns in RGB matrix
     *\return True, in case reading succeeded
     */
    bool read(std::istream& input);

    /*!
     *\brief Write data into the output stream - cols elements per rows rows
     *\param output Output stream
     *\param in     Input data to write
     *\param rows      Number of rows to write
     *\param cols      Number of columns to write
     *\return True, in case writing succeded
     */
    bool write(std::ostream& output) const;

    /*!
     *\brief Get array pointer
     *\return Image pointer instance
     */
    const float* getArray() const
    {
      return mArray;
    }

    /*!
     *\brief Get array size
     *\return Actual size
     */
    int getArraySize() const
    {
      return mSize;
    }

    
    /*!
     *\brief Set result value
     *\param res Result
     */
    void setResult(float result)
    {
      mResult = result;
    }

  private:
    //! Array pointer
    float* mArray;
    //! Array size
    int    mSize;
    //! Reduction result
    float  mResult;
  };

  

#endif // TASKSERIALIZABLE_H