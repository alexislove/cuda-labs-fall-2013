/*!
  *\file CpuTask.cpp
  *\brief This file contains CPU task implementation
 */

#include <cassert>
#include <memory>

#include <cuda.h>
#include <cuda_runtime.h>

#include <fstream>
#include <functional>

#include "CudaRuntimeWrapper.h"
#include "CpuProfiler.h"
#include "GpuProfiler.h"
#include "TaskSerializable.h"
#include "ITask.h"

#include "ReductionKernel.cuh"
#include "ReductionOptimizedKernel.cuh"

// This macro compares error code with CUDA_SUCCESS and in case smth failed - prints CUDA error message and returns false
#define CU_B_RETURN(e) if ((e) != CUDA_SUCCESS) { std::cout << cudaGetErrorString((e)) << std::endl; return false; }

// This macro executes given method and in case it fails returns false
#define B_RETURN(f) if (!(f)) { return false; }

#define IS_POW_2(n) (( (n) & ((n) - 1) ) == 0) 

#define BLOCK_SIZE 256
#define GRID_SIZE 64

struct KernelMap
{
  float* array;
  float* output;
};

class GpuTask : public ITask
{
public:
  GpuTask(bool optimized)
    : mDevice(NULL),
      mOptimized(optimized)
  {

  }

  virtual ~GpuTask() 
  {

  }

  virtual bool solve(const std::string& inputFileName, const std::string& outputFileName); 

private:

  bool initializeKernel(const float *array, int size, int gridSize);

  bool obtainOutput(float* result, int gridSize);

  bool releaseKernel();

  cudaError runReductionKernel(const float* array, int size, float* result);

  cudaError runKernel(const float* array, unsigned size, int blockSize, int gridSize, GpuProfiler* profiler);

  cudaError runOptimizedKernel(const float* array, unsigned size, int blockSize, int gridSize, GpuProfiler* profiler);

private:
  //! Device data map
  KernelMap *mDevice;
  //! Optimization usage state
  bool       mOptimized;
};

bool GpuTask::solve(const std::string& inputFileName, const std::string& outputFileName)
{
  CpuProfiler totalProfiler;
  CpuProfiler taskProfiler;

  if (!mOptimized)
  {
    std::cout << "******************GPU Task***************************" << std::endl;
  }
  else
  {
    std::cout << "******************GPU Optimized Task***************************" << std::endl;
  }

  std::shared_ptr<TaskSerializable> input(new TaskSerializable);
  totalProfiler.start();
  std::ifstream inputFile(inputFileName);
  B_RETURN(input->read(inputFile));
  
  taskProfiler.start();
  float result = 0.f;
  runReductionKernel(input->getArray(), input->getArraySize(), &result);
  taskProfiler.stop();

  std::shared_ptr<TaskSerializable> output(new TaskSerializable);

  output->setResult(result);
  std::ofstream outputFile(outputFileName);
  B_RETURN(output->write(outputFile));
  
  totalProfiler.stop();

  std::cout << "Total execution time:  " << totalProfiler.getMilliSeconds()   << " ms (" << totalProfiler.getNanoSeconds()            << " ns)" << std::endl;
  std::cout << "Task execution time:   " << taskProfiler.getMilliSeconds()    << " ms (" << taskProfiler.getNanoSeconds()             << " ns)" << std::endl;

  return true;
}

bool GpuTask::initializeKernel(const float *array, const int size, int gridSize)
{
  cudaError cuError = cudaSuccess;

  cuError = cudaSetDevice(0);
  CU_B_RETURN(cuError);

  mDevice = new KernelMap;

  const int arrayByteSize  = size * sizeof(float);
  const int outputByteSize = gridSize * sizeof(float);

  // Allocate memory on the device for source and target images
  cuError = cudaMalloc(&mDevice->array,  arrayByteSize);
  CU_B_RETURN(cuError);
  cuError = cudaMalloc(&mDevice->output, outputByteSize); 
  CU_B_RETURN(cuError);

  cuError = cudaMemset(mDevice->output, 0, outputByteSize);
  CU_B_RETURN(cuError);

  cuError = cudaMemcpy(mDevice->array, array, arrayByteSize, cudaMemcpyHostToDevice);
  CU_B_RETURN(cuError);
  

  return true;
}

bool GpuTask::obtainOutput(float* output, int gridSize)
{
  cudaError cuError = cudaSuccess;

  float* hostOutput = new float[gridSize];
  cuError = cudaMemcpy(hostOutput, mDevice->output, gridSize * sizeof(float), cudaMemcpyDeviceToHost);
  CU_B_RETURN(cuError);

  *output = 0.f;
  for (int idx = 0; idx < gridSize; ++idx)
  {
    *output += hostOutput[idx];
  }

  return true;
}

bool GpuTask::releaseKernel()
{
  cudaError cuError = cudaSuccess;

  cuError = cudaFree(mDevice->array);
  CU_B_RETURN(cuError);
  cuError = cudaFree(mDevice->output);
  CU_B_RETURN(cuError);

  delete mDevice;

  return true;
}

cudaError GpuTask::runReductionKernel(const float* array, int size, float* result)
{
  cudaError cuError = cudaSuccess;

  int deviceId = 0;
  if (!CudaRuntimeWrapper::getBestCudaDeviceId(&deviceId))
  {
    return cudaErrorDevicesUnavailable;
  }

  CudaRuntimeWrapper wrapper(deviceId);

  int blockSize = 0;
  int gridSize  = 0;
  mOptimized ? wrapper.getOptBlockAndGridSize2(size, BLOCK_SIZE, GRID_SIZE, &blockSize, &gridSize) :
               wrapper.getOptBlockAndGridSize(size, BLOCK_SIZE, GRID_SIZE, &blockSize, &gridSize);

  initializeKernel(array, size, gridSize);

  GpuProfiler gpuTaskProfiler;
  gpuTaskProfiler.initialize();

  if (mOptimized)
  {
    runOptimizedKernel(array, size, blockSize, gridSize, &gpuTaskProfiler);
  }
  else
  {
    runKernel(array, size, blockSize, gridSize, &gpuTaskProfiler);
  }
  cuError = cudaDeviceSynchronize();
  gpuTaskProfiler.stop();

  std::cout << "Kernel execution time: " << gpuTaskProfiler.getMilliseconds() << " ms (" << gpuTaskProfiler.getMilliseconds() * 10e6f << " ns)" << std::endl;

  gpuTaskProfiler.release();

  obtainOutput(result, gridSize);

  releaseKernel();

  return cuError;
}

cudaError GpuTask::runKernel(const float* array, unsigned size, int blockSize, int gridSize, GpuProfiler* profiler)
{
  int sharedMemoryByteSize = sizeof(float) * ((blockSize <= BS32) ? 2 * blockSize : blockSize);

  profiler->start();
  cuReductionKernel<<<gridSize, blockSize, sharedMemoryByteSize>>>(mDevice->array, size, mDevice->output);

  return cudaSuccess;
}

#define CASE_CALL_WITH_BLOCK_SIZE(s, isPow2) \
  case (s): \
    profiler->start(); \
    cuReductionOptimizedKernel<(s), (isPow2)><<<gridSize, blockSize, sharedMemoryByteSize>>>(mDevice->array, size, mDevice->output); \
    break

cudaError GpuTask::runOptimizedKernel(const float* array, unsigned size, int blockSize, int gridSize, GpuProfiler* profiler)
{
  int sharedMemoryByteSize = sizeof(float) * ((blockSize <= BS32) ? 2 * blockSize : blockSize);

  if (IS_POW_2(size))
  { 
    switch (blockSize)
    {
    CASE_CALL_WITH_BLOCK_SIZE(BS512, true);
    CASE_CALL_WITH_BLOCK_SIZE(BS256, true);
    CASE_CALL_WITH_BLOCK_SIZE(BS128, true);
    CASE_CALL_WITH_BLOCK_SIZE(BS64,  true);
    CASE_CALL_WITH_BLOCK_SIZE(BS32,  true);
    CASE_CALL_WITH_BLOCK_SIZE(BS16,  true);
    CASE_CALL_WITH_BLOCK_SIZE(BS8,   true);
    CASE_CALL_WITH_BLOCK_SIZE(BS4,   true);
    CASE_CALL_WITH_BLOCK_SIZE(BS2,   true);
    CASE_CALL_WITH_BLOCK_SIZE(BS1,   true);
    default:
      assert(!"Unsupported block size!");
      break;
    }
  }
  else
  {
    switch (blockSize)
    {
    CASE_CALL_WITH_BLOCK_SIZE(BS512, false);
    CASE_CALL_WITH_BLOCK_SIZE(BS256, false);
    CASE_CALL_WITH_BLOCK_SIZE(BS128, false);
    CASE_CALL_WITH_BLOCK_SIZE(BS64,  false);
    CASE_CALL_WITH_BLOCK_SIZE(BS32,  false);
    CASE_CALL_WITH_BLOCK_SIZE(BS16,  false);
    CASE_CALL_WITH_BLOCK_SIZE(BS8,   false);
    CASE_CALL_WITH_BLOCK_SIZE(BS4,   false);
    CASE_CALL_WITH_BLOCK_SIZE(BS2,   false);
    CASE_CALL_WITH_BLOCK_SIZE(BS1,   false);
    default:
      assert(!"Unsupported block size!");
      break;
    }
  }

  return cudaSuccess;
}

ITask* GCreateGPUTask()
{
  return new GpuTask(false);
}

ITask* GCreateGPUTaskOptimized()
{
  return new GpuTask(true);
}