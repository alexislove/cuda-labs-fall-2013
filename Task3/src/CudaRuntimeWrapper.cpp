/*!
 *\file CudaRuntimeWrapper.cpp
 *\brief This file contains CudaRuntimeWrapper helper class implementation
 */

#include <cassert>
#include <cmath>
#include <functional>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "CudaRuntimeWrapper.h"

#define CUDA_ERROR_DUMP_RETURN_IF(stream, str) if (error != cudaSuccess) { stream << str << std::endl; return; }
#define CUDA_ERROR_DUMP_RETURN_BOOL_IF(stream, str) if (error != cudaSuccess) { stream << str << std::endl; return false; }

#define LN2 
#define LOG2(x) std::logf(x) / std::logf(2.f)

namespace Private
{
  //! Get maximum number of blocks per streaming multiprocessor
  int getMaxBlocksPerSM(const cudaDeviceProp& devProps)
  {
    static const int cudaComputeCapabilityMajorMax = 3;
    static const int blocksCount[cudaComputeCapabilityMajorMax + 1] = {
      -1, // Dummy one
       8, // CCC 1.x
       8, // CCC 2.x
      16, // CCC 3.x
    };

    assert(devProps.major >= 1 && devProps.major <= 3 && "Unsupported CUDA Compute Capability version!");
    return blocksCount[devProps.major];
  }

} // namespace Private



/**
 public static:
 */
bool CudaRuntimeWrapper::getCudaDeviceCount(int* outCount)
{
  cudaError status = cudaGetDeviceCount(outCount);
  return status == cudaSuccess;
}

bool CudaRuntimeWrapper::getBestCudaDeviceId(int *outDeviceId)
{
  // Unfortunately can't test it, return only 0's device
  *outDeviceId = 0;
  return true;
}

/**
 public:
 */

CudaRuntimeWrapper::CudaRuntimeWrapper(CudaDeviceId deviceId)
  : mDeviceId(deviceId)
{

}

void CudaRuntimeWrapper::dumpCudaDeviceInfo(std::ostream& stream /* = std::cout */) const
{
  cudaError error = cudaSuccess;

  int driverVersion = 0;
  error = cudaDriverGetVersion(&driverVersion);
  CUDA_ERROR_DUMP_RETURN_IF(stream, "Unable to obtain CUDA device driver version!");

  int runtimeVersion = 0;
  error = cudaRuntimeGetVersion(&runtimeVersion);
  CUDA_ERROR_DUMP_RETURN_IF(stream, "Unable to obtain CUDA runtime version!");


  cudaDeviceProp devProps = { 0 };
  error = cudaGetDeviceProperties(&devProps, mDeviceId);
  CUDA_ERROR_DUMP_RETURN_IF(stream, "Unable to obtain CUDA device properties!");
  
  stream << " CUDA Driver Version:                        " << driverVersion / 1000  << "." << (driverVersion % 100) / 10  << std::endl;
  stream << " CUDA Runtime Version:                       " << runtimeVersion / 1000 << "." << (runtimeVersion % 100) / 10 << std::endl;

  stream << " CUDA Device Compute Capability Version:     " << devProps.major << "." << devProps.minor                     << std::endl;
  stream << " CUDA Warp Size:                             " << devProps.warpSize                                           << std::endl;
  stream << " CUDA Maximum Number Of Threads Per SM:      " << devProps.maxThreadsPerMultiProcessor                        << std::endl;
  stream << " CUDA Maximum Number Of Threads Per Block:   " << devProps.maxThreadsPerBlock                                 << std::endl;
  stream << " CUDA Maximum Number Of Registers Per Block: " << devProps.regsPerBlock                                       << std::endl;

  stream << " CUDA Constant Memory Amount:                " << devProps.totalConstMem                                      << std::endl;
  stream << " CUDA Global Memory Amount:                  " << devProps.totalGlobalMem                                     << std::endl;
  stream << " CUDA Shared Memory Per Block:               " << devProps.sharedMemPerBlock                                  << std::endl;
}

bool CudaRuntimeWrapper::getOptBlockAndGridSize(int inputSize, 
                                                int* outBlockSize, 
                                                int* outGridSize) const
{
  cudaError error = cudaSuccess;

  cudaDeviceProp devProps = { 0 };
  error = cudaGetDeviceProperties(&devProps, mDeviceId);
  CUDA_ERROR_DUMP_RETURN_BOOL_IF(std::cerr, "Unable to get CUDA device properties!");

  const int blocksPerSM                 = Private::getMaxBlocksPerSM(devProps);
  const int warpSize                    = devProps.warpSize;
  const int maxThreadsPerMultiProcessor = devProps.maxThreadsPerMultiProcessor;
  const int maxThreadsPerBlock          = devProps.maxThreadsPerBlock;

  int optBlockSize           = 0;
  int maxSimultaneousThreads = 0;

  const int iterationsCount = maxThreadsPerBlock / warpSize + 1;

  // If task is very small...
  if (warpSize >= inputSize)
  {
    *outBlockSize = inputSize;
    *outGridSize  = 1;
    return true;
  }

  // Try to find maximum occupancy of the SMs
  for (int idx = 1; idx < iterationsCount; ++idx)
  {
    const int blockSize = warpSize * idx;

    // Fairly enough threads
    if (blockSize >= inputSize)
    {
      optBlockSize = blockSize;
      break;
    }

    const int possibleBlocksCount = std::min(blocksPerSM, 
                                             maxThreadsPerMultiProcessor / blockSize);
    const int simultaneousThreads = possibleBlocksCount * blockSize;

    if (simultaneousThreads > maxSimultaneousThreads)
    {
      maxSimultaneousThreads = simultaneousThreads;
      optBlockSize           = blockSize;
      // Check, if we've already reached maximum number of threads - 
      // so that there's no need to waste time
      if (maxSimultaneousThreads == maxThreadsPerMultiProcessor)
      {
        break;
      }
    }
  }

  // Calculate grid size, depending on the block size
  *outGridSize  = static_cast<int>(std::ceilf(static_cast<float>(inputSize) / 
                                              static_cast<float>(optBlockSize))); 
  *outBlockSize = optBlockSize;

  return true;
}

bool CudaRuntimeWrapper::getOptBlockAndGridSize(int inputSize, int maxBlockSize, int maxGridSize, int* outBlockSize, int* outGridSize) const
{
  std::function<unsigned(unsigned)> np2 = [](unsigned n) -> unsigned {
    --n;
    n |= (n >> 1) | (n >> 2) | (n >> 4) | (n >> 8) | (n >> 16);
    return ++n;
  };

  int blockSize = (inputSize < maxBlockSize) ? np2(inputSize) : maxBlockSize; 
  int gridSize  = (inputSize + blockSize - 1) / blockSize; 

  *outBlockSize = blockSize;
  *outGridSize  = gridSize;

  return true;
}

bool CudaRuntimeWrapper::getOptBlockAndGridSize2(int inputSize, int maxBlockSize, int maxGridSize, int* outBlockSize, int* outGridSize) const
{
  std::function<unsigned(unsigned)> np2 = [](unsigned n) -> unsigned {
    --n;
    n |= (n >> 1) | (n >> 2) | (n >> 4) | (n >> 8) | (n >> 16);
    return ++n;
  };

  int blockSize = (inputSize < maxBlockSize * 2) ? np2((inputSize + 1)/ 2) : maxBlockSize;
  int gridSize  = (inputSize + (blockSize * 2 - 1)) / (blockSize * 2);

  *outBlockSize = blockSize;
  *outGridSize  = std::min(maxGridSize, gridSize);

  return true;
}

bool CudaRuntimeWrapper::get2DOptBlockAndGridSize(int inputSize, int* outBlockSize, int* outGridSize) const
{
  cudaError error = cudaSuccess;

  cudaDeviceProp devProps = { 0 };
  error = cudaGetDeviceProperties(&devProps, mDeviceId);
  CUDA_ERROR_DUMP_RETURN_BOOL_IF(std::cerr, "Unable to get CUDA device properties!");

  const int blocksPerSM                 = Private::getMaxBlocksPerSM(devProps);
  const int warpSize                    = devProps.warpSize;
  const int maxThreadsPerMultiProcessor = devProps.maxThreadsPerMultiProcessor;
  const int maxThreadsPerBlock          = devProps.maxThreadsPerBlock;

  int optBlockSize           = 0;
  int maxSimultaneousThreads = 0;

  const int iterationsCount = static_cast<const int>(LOG2(static_cast<float>(maxThreadsPerBlock)));

  // Try to find maximum occupancy of the SMs for square dimensions
  for (int idx = 0; idx <= iterationsCount; ++idx)
  {
    const int blockSize1D = (2 << idx);//static_cast<const int>(std::powf(2.f, 2.f * idx)); // I don't want to calculate formula through bit shift
    const int blockSize2  = blockSize1D * blockSize1D;

    // Fairly enough threads
    if (blockSize2 >= inputSize)
    {
      optBlockSize = blockSize1D;
      break;
    }

    const int possibleBlocksCount = std::min(blocksPerSM, 
                                             maxThreadsPerMultiProcessor / blockSize2);
    const int simultaneousThreads = possibleBlocksCount * blockSize2;

    if (simultaneousThreads > maxSimultaneousThreads)
    {
      maxSimultaneousThreads = simultaneousThreads;
      optBlockSize           = blockSize1D;
      // Check, if we've already reached maximum number of threads - 
      // so that there's no need to waste time
      if (maxSimultaneousThreads == maxThreadsPerMultiProcessor)
      {
        break;
      }
    }
  }

  // Calculate grid size, depending on the block size
  *outGridSize  = static_cast<int>(std::ceilf(static_cast<float>(inputSize) / 
                                   static_cast<float>(optBlockSize * optBlockSize))); 
  *outBlockSize = optBlockSize;

  return true;
}


