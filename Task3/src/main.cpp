/*!
 *\file main.cpp
 *\brief Application entry point
 */

#include <memory>

#include "InputGenerator.h"
#include "ITask.h"
#include "TaskFactory.h"

#define PROFILE

int main(int argc, char* argv[])
{
  const std::string inputFileName("input524288.txt");
  // Uncomment this to generate input file with desired dimensions 
  InputGenerator::doRandfJob(inputFileName, 524288);
  
#ifdef PROFILE
  for (int idx = 0; idx < 3; ++idx)
  {
#endif // PROFILE

  
    // Uncomment this to enable CPU solution
    std::shared_ptr<ITask> cpuTask(GCreateCPUTask());
    if (!cpuTask->solve(inputFileName, "outputcpu.txt"))
    {
      return -1;
    }
  
    std::shared_ptr<ITask> gpuTask(GCreateGPUTask());
    if (!gpuTask->solve(inputFileName, "outputgpu.txt"))
    {
      return -1;
    }

    std::shared_ptr<ITask> gpuTaskOpt(GCreateGPUTaskOptimized());
    if (!gpuTaskOpt->solve(inputFileName, "outputgpuopt.txt"))
    {
      return -1;
    }

#ifdef PROFILE
  }
#endif // PROFILE

  return 0;
}