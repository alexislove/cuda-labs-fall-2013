/*!
 *\file ReductionOptimizedKernel.cuh
 *\brief This file contains implementation of optimized reduction kernel
 */

#ifndef REDUCTIONOPTIMIZEDKERNEL_CUH
  #define REDUCTIONOPTIMIZEDKERNEL_CUH
  
  // Wrapper macro for fast integer multiplication
  #define IMUL(x, y) (__mul24((x), (y)))

  #define BS1    1
  #define BS2    2
  #define BS4    4
  #define BS8    8
  #define BS16   16
  #define BS32   32
  #define BS64   64
  #define BS128  128
  #define BS256  256
  #define BS512  512

  #define LOAD_MORE(size) \
     if (blockSize >= (size)) \
     { \
        if (tx < (size) / 2) \
        { \
          sCache[tx] = sum = sum + sCache[tx + (size) / 2]; \
        } \
        __syncthreads(); \
    } 

  #define LOAD_SMALL(size) \
    if (blockSize >= (size)) \
    { \
      sCachePtr[tx] = sum = sum + sCachePtr[tx + (size) / 2]; \
    }

  /*!
   *\brief Kernel for sum reduction computation, template parameters
   *       used for compile-time unused code elimination, for example in 
   *       unreachable branches. And also unroll loops and avoid syncthreads.
   *\param image     Source array
   *\param size      Size of the source array
   *\param output    Output sum per each block
   */
  template <int blockSize, bool isPow2Size>
  __global__ void cuReductionOptimizedKernel(const float* array,
                                    int          size,
                                    float* const output)
  {
    extern __shared__ float sCache[]; 

    const int tx = threadIdx.x;
    const int bx = blockIdx.x; 
    const int bw = blockDim.x;

    const int gridSize  = IMUL(IMUL(blockSize, gridDim.x), 2);

    int loadIndex = IMUL(IMUL(bx, bw), 2) + tx;


    float sum = 0.f;

    for (; loadIndex < size; loadIndex += gridSize)
    {
      sum += array[loadIndex];
      if (isPow2Size || loadIndex + blockSize < size)
      {
        sum += array[loadIndex + blockSize];
      }
    }

    sCache[tx] = sum;
    __syncthreads();

    // Next branches will be removed in compile time, depending on the template param blockSize
    LOAD_MORE(BS512);
    LOAD_MORE(BS256);
    LOAD_MORE(BS128);
    
    if (tx < BS32)
    {
      volatile float* sCachePtr = sCache;
      LOAD_SMALL(BS64);
      LOAD_SMALL(BS32);
      LOAD_SMALL(BS16);
      LOAD_SMALL(BS8);
      LOAD_SMALL(BS4);
      LOAD_SMALL(BS2);
    }

    if (tx == 0)
    {
      float v = sCache[0];
      output[blockIdx.x] = v;
    }
  }


#endif // REDUCTIONKERNEL_CUH