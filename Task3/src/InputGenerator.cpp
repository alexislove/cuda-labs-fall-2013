/*!
 *\file InputGenerator.cpp
 *\brief This file contains InputGenerator class definition
 */

#include <cassert>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <future>

#include "InputGenerator.h"

namespace
{
  // Get random number from 0.0 to 1.0 inclusive
  float randf01()
  {
    return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
  }

  // Get random float number
  float randf(int rangeMax)
  {
    return static_cast<float>(rand() % rangeMax) * randf01();
  }

  // Write random floating point array
  void dumpRandfArray(std::ostream& file, int size, int rangeMax)
  {
    for (int idx = 0; idx < size; ++idx)
    {
      float element = randf(rangeMax);

      file << element;

      if (idx != size - 1)
      {
        file << ' ';
      }
    }
  }

  // Write random floating point array
  void dumpRandiArray(std::ostream& file, int size, int rangeMax)
  {
    for (int idx = 0; idx < size; ++idx)
    {
      int element = rand() % rangeMax;

      file << element;

      if (idx != size - 1)
      {
        file << ' ';
      }
    }
  }
}

void InputGenerator::doRandfJob(const std::string& fileName, int size)
{
  static const int cRangeMax = 1024;

  std::ofstream file(fileName);

  assert(file.is_open());

  file << size << std::endl;

  // Write image matrix
  dumpRandfArray(file, size, cRangeMax); // Some dummy max value range
  file << std::endl;
  // Flush file buffer
  file.flush();
  file.close();
}

void InputGenerator::doRandiJob(const std::string& fileName, int size)
{
  static const int cRangeMax = 1024;

  std::ofstream file(fileName);

  assert(file.is_open());

  file << size << std::endl;

  // Write image matrix
  dumpRandiArray(file, size, cRangeMax); // Some dummy max value range
  file << std::endl;
  // Flush file buffer
  file.flush();
  file.close();
}