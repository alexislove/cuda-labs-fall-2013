/*!
 *\file InputGenerator.h
 *\brief This file contains InputGenerator class definition
 */

#ifndef INPUTGENERATOR_H
  #define INPUTGENERATOR_H

  #include <string>

  /*!
   *\class InputGenerator 
   *\brief This class represents input data generator for the specified task
   */
  struct InputGenerator
  {
    /*!
     *\brief Perform input data generation
     *\param fileName Desired file name
     *\param size Array size to generate
     */
    static void doRandfJob(const std::string& fileName, int size);

    /*!
     *\brief Perform input data generation
     *\param fileName Desired file name
     *\param size Array size to generate
     */
    static void doRandiJob(const std::string& fileName, int size);

  };

#endif // INPUTGENERATOR_H