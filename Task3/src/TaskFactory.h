/*! 
 *\file TaskFactory.h
 *\brief This file contains extern task factory routines definition
 */

#ifndef TASKFACTORY_H
  #define TASKFACTORY_H

  struct ITask;
  
  /*! 
   *\brief Create CPU task instance
   *\return CPU task instance pointer, caller takes ownership of it
   */
  extern ITask* GCreateCPUTask();

  /*! 
   *\brief Create GPU task instance
   *\return GPU task instance pointer, caller takes ownership of it
   */
  extern ITask* GCreateGPUTask();

  /*! 
   *\brief Create GPU task with optimizations instance
   *\return GPU task instance pointer, caller takes ownership of it
   */
  extern ITask* GCreateGPUTaskOptimized();

#endif // TASKFACTORY_H