/*!
 *\file Convolution.cuh
 *\brief This file contains host routine to run CUDA kernel
 */

#ifndef CONVOLUTION_CUH
  #define CONVOLUTION_CUH

  #include <cuda_runtime.h>

  #define MAX_MASK_DIM 9

  //! Convolution mask array, that should be stored in the constant memory
  //__device__ __constant__ float gConstConvolutionMask[MAX_MASK_DIM * MAX_MASK_DIM];

  /*!
   *\brief This routines starts convolution CUDA kernel with global memory usage
   *\param image Image matrix
   *\param mask Mask matrix
   *\param imageDim Image dimension
   *\param maskDim Mask dimension
   *\return CUDA error code
   */
  //cudaError GRunConvolutionGlobalMemoryKernel(const float* matrix, int imageDim, int maskDim, float* const out);
  

#endif // CONVOLUTION_CUH