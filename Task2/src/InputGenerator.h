/*!
 *\file InputGenerator.h
 *\brief This file contains InputGenerator class definition
 */

#ifndef INPUTGENERATOR_H
  #define INPUTGENERATOR_H

  #include <string>

  /*!
   *\class InputGenerator 
   *\brief This class represents input data generator for the specified task
   */
  struct InputGenerator
  {
    /*!
     *\brief Perform input data generation
     *\param fileName Desired file name
     *\param imageDim Dimensions of the image to be generated
     *\param maskDim Dimensions of the mask to be generated
     */
    static void doJob(const std::string& fileName, int imageDim, int maskDim);

  };

#endif // INPUTGENERATOR_H