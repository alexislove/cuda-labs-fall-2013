/*!
 *\file TaskSerializable.h
 *\brief This file contains input/output serialization class definition
 */

#ifndef TASKSERIALIZABLE_H
  #define TASKSERIALIZABLE_H

  #include <iostream>
  
  /*!
   *\class TaskSerializable
   *\brief This class represents input/output data serialization object
   */
  class TaskSerializable
  {
  public:
    TaskSerializable();

    ~TaskSerializable();

    /*!
     *\brief Read data from the input stream to the output arrays for the matrix and the mask
     *\param input Input stream
     *\param image Image array (not initialized)
     *\param mask  Convolution mask
     *\param rows     Number of rows in RGB matrix
     *\param cols     Number of columns in RGB matrix
     *\return True, in case reading succeeded
     */
    bool read(std::istream& input);

    /*!
     *\brief Write data into the output stream - cols elements per rows rows
     *\param output Output stream
     *\param in     Input data to write
     *\param rows      Number of rows to write
     *\param cols      Number of columns to write
     *\return True, in case writing succeded
     */
    bool write(std::ostream& output) const;

    /*!
     *\brief Get image pointer
     *\return Image pointer instance
     */
    const float* getPseudoImage()
    {
      return mPseudoImage;
    }

    /*!
     *\brief Get pseudo image dimensions
     *\param ow Output width
     *\param oh Output height
     */
    void getPseudoImageDimensions(int* ow, int* oh) const
    {
      *ow = mPseudoImageWidth;
      *oh = mPseudoImageHeight;
    }

    /*!
     *\brief Get convolution mask pointer
     *\return Mask data pointer
     */
    const float* getConvolutionMask() const
    {
      return mConvolutionMask;
    }
    
    /*!
     *\brief Get convolution mask dimensions
     *\param ow Output mask width
     *\param oh Output mask height
     */
    void getConvolutionMaskDimensions(int* ow, int* oh) const
    {
      *ow = mConvolutionMaskWidth;
      *oh = mConvolutionMaskHeight;
    }

    /*!
     *\brief Set pseudo image data and take ownership
     *\param data Image data
     */
    void setPseudoImage(float* data, int width, int height)
    {
      if (mPseudoImage)
      {
        delete[] mPseudoImage;
      }
      mPseudoImage       = data;
      mPseudoImageWidth  = width;
      mPseudoImageHeight = height;
    }

    /*!
     *\brief Set convolution mask
     *\param data Mask data
     *\param width Mask width
     *\param height Mask height
     */
    void setConvolutionMask(float* data, int width, int height)
    {
      if (mConvolutionMask)
      {
        delete[] mConvolutionMask;
      }
      mConvolutionMask       = data;
      mConvolutionMaskWidth  = width;
      mConvolutionMaskHeight = height;
    }

  private:
    //! Source pseudo image
    float* mPseudoImage;
    //! Pseudo image width
    int    mPseudoImageWidth;
    //! Pseudo image height
    int    mPseudoImageHeight;
    //! Source convolution mask
    float* mConvolutionMask;
    //! Convolution mask width
    int    mConvolutionMaskWidth;
    //! Convolution mask height
    int    mConvolutionMaskHeight;
  };

  

#endif // TASKSERIALIZABLE_H