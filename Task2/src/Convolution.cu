/*!
 *\file Kernel.cu
 *\brief This file contains CUDA kernel implementation
 */

#include <cmath>

#include "CudaRuntimeWrapper.h"

#include "Convolution.cuh"

