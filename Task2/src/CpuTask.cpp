/*!
 *\file CpuTask.cpp
 *\brief This file contains CPU task implementation
 */

#include <chrono>
#include <fstream>

#include "CpuProfiler.h"
#include "TaskSerializable.h"

#include "ITask.h"

class CpuTask : public ITask
{
public:
  virtual ~CpuTask() 
  {

  }

  virtual bool solve(const std::string& inputFileName, const std::string& outputFileName); 

};

bool CpuTask::solve(const std::string& inputFileName, const std::string& outputFileName)
{
  CpuProfiler totalProfiler;
  CpuProfiler taskProfiler;

  std::cout << "******************CPU Task***************************" << std::endl;

  return [&outputFileName, &taskProfiler, &totalProfiler] (TaskSerializable* output) -> bool {
    if (!output) {
      return false;
    }
    bool ret = output->write(std::ofstream(outputFileName));
    totalProfiler.stop();

    std::cout << "Total execution time:  " << totalProfiler.getMilliSeconds()   << " ms (" << totalProfiler.getNanoSeconds()            << " ns)" << std::endl;
    std::cout << "Task execution time:   " << taskProfiler.getMilliSeconds()    << " ms (" << taskProfiler.getNanoSeconds()             << " ns)" << std::endl;

    return (delete output, ret);
  } ([&taskProfiler] (TaskSerializable* input) -> TaskSerializable* {
    if (!input) {
      return nullptr;
    }

    taskProfiler.start();

    const float* image = input->getPseudoImage();
    int imageWidth = 0, imageHeight = 0;
    input->getPseudoImageDimensions(&imageWidth, &imageHeight);

    const float* mask = input->getConvolutionMask();
    int maskWidth = 0, maskHeight = 0;
    input->getConvolutionMaskDimensions(&maskWidth, &maskHeight);

    float* result = new float[imageWidth * imageHeight];

    const int halfMaskW = maskWidth / 2;
    const int halfMaskH = maskHeight / 2;

    const int halfMaskWE = maskWidth / 2 + maskWidth % 2;
    const int halfMaskHE = maskHeight / 2 + maskHeight % 2;

    for (int i = 0; i < imageHeight; ++i) {
      for (int j = 0; j < imageWidth; ++j) {
        const int resIndex = i * imageWidth + j;
        result[resIndex] = 0.f;
        for (int k = -halfMaskH; k < halfMaskHE; ++k) {
          if ((i + k < 0) || (i + k >= imageHeight)) {
            continue;
          }
          
          for (int l = -halfMaskW; l < halfMaskWE; ++l) {
            if ((j + l < 0) || (j + l >= imageWidth)) {
              continue;
            }
            const int imageIndex = (i + k) * imageWidth + (j + l);
            const int maskIndex  = (k + halfMaskH) * maskWidth + (l + halfMaskW);
            result[resIndex] += image[imageIndex] * mask[maskIndex];
          }
        }
      }
    }

    TaskSerializable* output = new TaskSerializable;
    output->setPseudoImage(result, imageWidth, imageHeight);

    taskProfiler.stop();

    return (delete input, output);
  } ([&inputFileName, &totalProfiler] () -> TaskSerializable* { 
    TaskSerializable* task = new TaskSerializable;
    totalProfiler.start();
    return ((task->read(std::ifstream(inputFileName))) ? task : (delete task, nullptr));
  } ()));
}

ITask* GCreateCPUTask()
{
  return new CpuTask;
}

