/*!
 *\file ConvolutionGlobalMemory.cuh
 *\brief This file contains naive implementation of matrix convolution using global memory
 */

#ifndef CONVOLUTIONGLOBALMEMORY_CUH
  #define CONVOLUTIONGLOBALMEMORY_CUH
  
  #define IMUL(x, y) __mul24((x), (y))

  /*!
   *\brief Kernel for convolution computation using global memory
   *\param image           Source image data
   *\param imageDim        Image matrix dimensions
   *\param maskDim         Mask matrix dimensions
   *\param maskRadiusStart First index of mask array, that mask matrix should be applied from (optimization - precomputed before call)
   *\param maskRadiusEnd   Last index of mask array, that mask matrix should be applied from (optimization - precomputed before call)
   *\param output          Output matrix
   */
  __global__ void cuConvolutionGlobalMemoryKernel(const float* image, 
                                                int imageDim, 
                                                int maskDim,
                                                int maskRadiusStart, 
                                                int maskRadiusEnd, 
                                                float* const output)
  {
    const int row         = IMUL(blockIdx.y, blockDim.y) + threadIdx.y;
    const int col         = IMUL(blockIdx.x, blockDim.x) + threadIdx.x;

    if (col < imageDim && row < imageDim)
    {
      float result = 0.f;
  #pragma unroll
      for (int k = -maskRadiusStart; k < maskRadiusEnd; ++k) // Per mask row
      {
        const int aRow = row + k;
        if (aRow >= 0 && aRow < imageDim)
        {
  #pragma unroll
          for (int l = -maskRadiusStart; l < maskRadiusEnd; ++l) // Per mask column
          {
            const int aCol = col + l;
            if (aCol >= 0 && aCol < imageDim)
            {
              const int imageIndex = IMUL(aRow, imageDim) + aCol;
              const int maskIndex  = IMUL((k + maskRadiusStart), maskDim) + (l + maskRadiusStart); // Mask width = 2 * mask radius
              result += image[imageIndex] * gConstConvolutionMask[maskIndex];
            }
          }
        }
      }
      const int resultIndex = IMUL(row, imageDim) + col;
      output[resultIndex]   = result;
    }
  }


#endif // CONVOLUTIONGLOBALMEMORY_CUH