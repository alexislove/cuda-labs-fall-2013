/*!
 *\file ConvolutionSharedMemory.cuh
 *\brief This file contains implementation of matrix convolution using shared memory
 */

#ifndef CONVOLUTIONSHAREDMEMORY_CUH
  #define CONVOLUTIONSHAREDMEMORY_CUH
  
  // Wrapper macro for fast integer multiplication
  #define IMUL(x, y) __mul24((x), (y))

  #define SMEMI(x, y) IMUL((y), tileDim) + (x)

  // Wrapper macro for shared memory access, assumes smem array is called sTiles and there's a variable tileDim
  #define SMEM(x, y) sTiles[IMUL((y), tileDim) + (x)]

  __device__ float pixel(const float* image, int x, int y, int dim)
  {
    if (x < 0 || y < 0)
    {
      return 0.f;
    }
    if (x >= dim || y >= dim)
    {
      return 0.f;
    }
    
    return image[IMUL(y, dim) + x];
  }

  /*!
   *\brief Kernel for convolution computation using shared memory
   *\param image           Source image data
   *\param imageDim        Image matrix dimensions
   *\param maskDim         Mask matrix dimensions
   *\param maskRadius      Radius of the mask (optimization - precomputed before call)
   *\param maskRadiusEnd   Last index of mask array, that mask matrix should be applied from (optimization - precomputed before call)
   *\param tileDim         Input data tile dimensions       
   *\param output          Output matrix
   */
  __global__ void cuConvolutionSharedMemoryKernel(const float* image, 
                                                  int imageDim, 
                                                  int maskDim,
                                                  int maskRadius, 
                                                  int maskRadiusEnd, 
                                                  int tileDim,
                                                  float* const output)
  {
    extern __shared__ float sTiles[]; // Size is: (tileDim + maskRadius) ^ 2, indexed using threadIdx.y * tileDim + threadIdx.x

    const int tx = threadIdx.x;
    const int ty = threadIdx.y;
    const int bx = blockIdx.x;
    const int by = blockIdx.y;
    const int bw = blockDim.x;
    const int bh = blockDim.y;

    const int x = IMUL(bx, bw) + tx;
    const int y = IMUL(by, bh) + ty;

    const int imageIndex = IMUL(y, imageDim) + x;

    int  tboW = bw;
    int  tboH = bh;

    // Center region
    SMEM(maskRadius + tx, maskRadius + ty) = pixel(image, x, y, imageDim);

    // Borders
    if (tx < maskRadius)
    {
      // Left
      SMEM(tx, maskRadius + ty)                     = pixel(image, x - maskRadius, y, imageDim);
      // Right
      SMEM(maskRadius + tboW + tx, maskRadius + ty) = pixel(image, x + tboW, y, imageDim);
    }
    if (ty < maskRadius)
    {
      // Top
      SMEM(maskRadius + tx, ty)                     = pixel(image, x, y - maskRadius, imageDim);
      // Bottom
      SMEM(maskRadius + tx, maskRadius + tboH + ty) = pixel(image, x, y + tboH, imageDim);
    }

    // Load corners
    if (tx < maskRadius && ty < maskRadius)
    {
      // Top-left
      SMEM(tx,                   ty)                       = pixel(image, x - maskRadius, y - maskRadius, imageDim);
      // Bottom-left
      SMEM(tx,                   maskRadius + tboH + ty)   = pixel(image, x - maskRadius, y + tboH, imageDim);
      // Top-right
      SMEM(maskRadius + tboW + tx, ty)                     = pixel(image, x + tboW, y - maskRadius, imageDim);
      // Bottom-right
      SMEM(maskRadius + tboW + tx, maskRadius + tboH + ty) = pixel(image, x + tboW, y + tboH, imageDim);
    }

    __syncthreads();

    
    // Calculate convolution, without divergence branching
    const int x0 = maskRadius + tx;
    const int y0 = maskRadius + ty;
          
    float result = 0.f; // Accumulate result, avoiding DRAM accesses each step
    //#pragma unroll
    for (int k = -maskRadius; k < maskRadiusEnd; ++k) // Per mask row
    {
      //#pragma unroll
      for (int l = -maskRadius; l < maskRadiusEnd; ++l) // Per mask column
      {            
        const int maskIndex = IMUL((k + maskRadius), maskDim) + (l + maskRadius);
        
        const float tile = SMEM(x0 + l, y0 + k);

        result += tile * gConstConvolutionMask[maskIndex];       
      }
    }
      
    if (x < imageDim && y < imageDim)
    {
      output[imageIndex] = result;
    }
  }


#endif // CONVOLUTIONGLOBALMEMORY_CUH