/*!
 *\file main.cpp
 *\brief Application entry point
 */

#include <memory>

#include "InputGenerator.h"
#include "ITask.h"
#include "TaskFactory.h"

#define PROFILE
//#define ONLY_GENERATE

int main(int argc, char* argv[])
{
  const std::string inputFileName("input4096x3.txt");
  // Uncomment this to generate input file with desired dimensions 
  //InputGenerator::doJob(inputFileName, 4096, 9);

#ifndef ONLY_GENERATE

#ifdef PROFILE
  for (int idx = 0; idx < 3; ++idx)
  {
#endif // PROFILE

  
    // Uncomment this to enable CPU solution
    std::shared_ptr<ITask> cpuTask(GCreateCPUTask());
    if (!cpuTask->solve(inputFileName, "outputcpu.txt"))
    {
      return -1;
    }
  

    std::shared_ptr<ITask> gpuGlobalMemoryTask(GCreateGPUTaskGlobalMemory());

    if (!gpuGlobalMemoryTask->solve(inputFileName, "outputgpuglobal.txt"))
    {
      return -1;
    }

    std::shared_ptr<ITask> gpuSharedMemoryTask(GCreateGPUTaskSharedMemory());

    if (!gpuSharedMemoryTask->solve(inputFileName, "outputgpushared.txt"))
    {
      return -1;
    }
#ifdef PROFILE
  }
#endif // PROFILE

#endif // ONLY_GENERATE

  return 0;
}