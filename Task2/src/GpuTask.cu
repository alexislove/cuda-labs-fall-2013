/*!
  *\file CpuTask.cpp
  *\brief This file contains CPU task implementation
 */

#include <memory>

#include <cuda.h>
#include <cuda_runtime.h>

#include <fstream>

#include "CudaRuntimeWrapper.h"
#include "CpuProfiler.h"
#include "GpuProfiler.h"
#include "TaskSerializable.h"
#include "ITask.h"

// This macro compares error code with CUDA_SUCCESS and in case smth failed - prints CUDA error message and returns false
#define CU_B_RETURN(e) if ((e) != CUDA_SUCCESS) { std::cout << cudaGetErrorString((e)) << std::endl; return false; }

// This macro executes given method and in case it fails returns false
#define B_RETURN(f) if (!(f)) { return false; }

#define MAX_MASK_DIM 9

// Declare constant memory array for the mask
__constant__ float gConstConvolutionMask[MAX_MASK_DIM * MAX_MASK_DIM];

/**
 External linkage is not supported, so kernel code is placed here...
 */
#pragma region CUDA global memory kernel

#define GLOBAL_MEM_BLOCK_SIZE 16

#include "ConvolutionGlobalMemory.cuh"

cudaError GRunConvolutionGlobalMemoryKernel(const float* image, int imageDim, int maskDim, float* const out)
{
  cudaError cuError = cudaSuccess;

  int deviceId = 0;
  if (!CudaRuntimeWrapper::getBestCudaDeviceId(&deviceId))
  {
    return cudaErrorDevicesUnavailable;
  }

  CudaRuntimeWrapper wrapper(deviceId);

  int blockDim = 0;
  int gridDim  = 0;

  // No need to:
  // wrapper.get2DOptBlockAndGridSize(imageDim * imageDim, &blockDim, &gridDim);
  if (imageDim < GLOBAL_MEM_BLOCK_SIZE)
  {
    blockDim = imageDim; // Don't waste threads, small images are inpractical
    gridDim  = 1;
  }
  else
  {
    blockDim = GLOBAL_MEM_BLOCK_SIZE;
    gridDim  = static_cast<int>(std::ceilf(static_cast<float>(imageDim) / 
                                           static_cast<float>(blockDim))); 
  }

  std::cout << gridDim << std::endl;

  const dim3 gridSize(gridDim, gridDim, 1U);
  const dim3 blockSize(blockDim, blockDim, 1U);

  const int maskRadius = maskDim / 2;
  const int endRadius  = maskDim / 2 + maskDim % 2;
  cuConvolutionGlobalMemoryKernel<<<gridSize, blockSize>>>(image, imageDim, maskDim, maskRadius, endRadius, out);

  cuError = cudaDeviceSynchronize();

  return cuError;
}

#pragma endregion

#pragma region CUDA shared memory kernel

#include "ConvolutionSharedMemory.cuh"

#define SHARED_MEM_BLOCK_SIZE 16

cudaError GRunConvolutionSharedMemoryKernel(const float* image, int imageDim, int maskDim, float* const out)
{
  cudaError cuError = cudaSuccess;

  int deviceId = 0;
  if (!CudaRuntimeWrapper::getBestCudaDeviceId(&deviceId))
  {
    return cudaErrorDevicesUnavailable;
  }

  CudaRuntimeWrapper wrapper(deviceId);

  const int maskRadius = maskDim / 2;
  const int endRadius  = maskRadius + maskDim % 2;

  int blockDim = 0;
  int gridDim  = 0;
  if (imageDim < SHARED_MEM_BLOCK_SIZE)
  {
    blockDim = imageDim; // Don't waste threads, small images are inpractical
    gridDim  = 1;
  }
  else
  {
    blockDim = SHARED_MEM_BLOCK_SIZE;
    gridDim  = static_cast<int>(std::ceilf(static_cast<float>(imageDim) / 
                                           static_cast<float>(blockDim))); 
  }

  std::cout << gridDim << std::endl;

  const dim3 gridSize(gridDim, gridDim, 1U);
  const dim3 blockSize(blockDim, blockDim, 1U);

  const int tileDim = blockDim + ((maskDim % 2 == 0) ? maskDim : 2 * maskRadius);
 
  const int sharedMemoryByteSize = tileDim * tileDim * sizeof(float);

  cuConvolutionSharedMemoryKernel<<<gridSize, blockSize, sharedMemoryByteSize>>>(image, 
                                                                                 imageDim, 
                                                                                 maskDim, 
                                                                                 maskRadius, 
                                                                                 endRadius, 
                                                                                 tileDim, 
                                                                                 out);

  cuError = cudaDeviceSynchronize();

  return cuError;
}

#pragma endregion

struct KernelMap
{
  float* image;
  float* output;
};

class GpuTask : public ITask
{
public:
  GpuTask(bool useSharedMemory)
    : mDevice(NULL),
      mUseSharedMemory(useSharedMemory)
  {

  }

  virtual ~GpuTask() 
  {

  }

  virtual bool solve(const std::string& inputFileName, const std::string& outputFileName); 

private:

  bool initializeKernel(const float *image, const float* mask, int imageDim, int maskDim);

  bool obtainOutput(int imageDim, float **output);

  bool releaseKernel();

private:
  //! Device data map
  KernelMap *mDevice;
  //! Shared memory kernel usage state
  bool       mUseSharedMemory;
};

bool GpuTask::solve(const std::string& inputFileName, const std::string& outputFileName)
{
  CpuProfiler totalProfiler;
  CpuProfiler taskProfiler;
  GpuProfiler gpuTaskProfiler;

  if (mUseSharedMemory)
  {
    std::cout << "******************GPU Shared Memory Task***************************" << std::endl;
  }
  else
  {
    std::cout << "******************GPU Global Memory Task***************************" << std::endl;
  }

  gpuTaskProfiler.initialize();

  std::shared_ptr<TaskSerializable> input(new TaskSerializable);
  totalProfiler.start();
  std::ifstream inputFile(inputFileName);
  B_RETURN(input->read(inputFile));
  
  taskProfiler.start();
  // Initialize CUDA and copy data to device memory
  int imageDim = 0, dummy = 0;
  int maskDim  = 0;
  input->getPseudoImageDimensions(&imageDim, &dummy);
  input->getConvolutionMaskDimensions(&maskDim, &dummy);
  B_RETURN(initializeKernel(input->getPseudoImage(), input->getConvolutionMask(), imageDim, maskDim));
  
  gpuTaskProfiler.start();
  // Run kernel on the input data
  if (mUseSharedMemory)
  {
    CU_B_RETURN(GRunConvolutionSharedMemoryKernel(mDevice->image, imageDim, maskDim, mDevice->output));
  }
  else
  {
    CU_B_RETURN(GRunConvolutionGlobalMemoryKernel(mDevice->image, imageDim, maskDim, mDevice->output));
  }
  
  gpuTaskProfiler.stop();

  float *outImage = NULL;
  B_RETURN(obtainOutput(imageDim, &outImage));
  
  B_RETURN(releaseKernel());
  
  taskProfiler.stop();

  std::shared_ptr<TaskSerializable> output(new TaskSerializable);

  output->setPseudoImage(outImage, imageDim, imageDim);
  std::ofstream outputFile(outputFileName);
  B_RETURN(output->write(outputFile));
  
  totalProfiler.stop();

  std::cout << "Total execution time:  " << totalProfiler.getMilliSeconds()   << " ms (" << totalProfiler.getNanoSeconds()            << " ns)" << std::endl;
  std::cout << "Task execution time:   " << taskProfiler.getMilliSeconds()    << " ms (" << taskProfiler.getNanoSeconds()             << " ns)" << std::endl;
  std::cout << "Kernel execution time: " << gpuTaskProfiler.getMilliseconds() << " ms (" << gpuTaskProfiler.getMilliseconds() * 10e6f << " ns)" << std::endl;

  gpuTaskProfiler.release();

  return true;
}

bool GpuTask::initializeKernel(const float *image, const float* mask, int imageDim, int maskDim)
{
  cudaError cuError = cudaSuccess;

  cuError = cudaSetDevice(0);
  CU_B_RETURN(cuError);

  mDevice = new KernelMap;

  const int imageByteSize = imageDim * imageDim * sizeof(float);
  const int maskByteSize  = maskDim * maskDim * sizeof(float);

  // Allocate memory on the device for source and target images
  cuError = cudaMalloc(&mDevice->image,  imageByteSize);
  CU_B_RETURN(cuError);
  cuError = cudaMalloc(&mDevice->output, imageByteSize);
  CU_B_RETURN(cuError);

  cuError = cudaMemset(mDevice->output, 0, imageByteSize);
  CU_B_RETURN(cuError);

  cuError = cudaMemcpy(mDevice->image, image, imageByteSize, cudaMemcpyHostToDevice);
  CU_B_RETURN(cuError);
  
  // Copy mask to the constant memory
  cuError = cudaMemcpyToSymbol(gConstConvolutionMask, mask, maskByteSize);
  CU_B_RETURN(cuError);

  return true;
}

bool GpuTask::obtainOutput(int imageDim, float **output)
{
  cudaError cuError = cudaSuccess;

  const int imageSize      = imageDim * imageDim;
  const int imageBytesSize = imageSize * sizeof(float);
  *output = new float[imageSize];

  cuError = cudaMemcpy(*output, mDevice->output, imageBytesSize, cudaMemcpyDeviceToHost);
  CU_B_RETURN(cuError);

  return true;
}

bool GpuTask::releaseKernel()
{
  cudaError cuError = cudaSuccess;

  cuError = cudaFree(mDevice->image);
  CU_B_RETURN(cuError);
  cuError = cudaFree(mDevice->output);
  CU_B_RETURN(cuError);

  delete mDevice;

  return true;
}

ITask* GCreateGPUTaskGlobalMemory()
{
  return new GpuTask(false);
}

ITask* GCreateGPUTaskSharedMemory()
{
  return new GpuTask(true);
}