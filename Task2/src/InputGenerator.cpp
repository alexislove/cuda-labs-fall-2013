/*!
 *\file InputGenerator.cpp
 *\brief This file contains InputGenerator class definition
 */

#include <cassert>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <future>

#include "InputGenerator.h"

namespace
{
  // Get random number from 0.0 to 1.0 inclusive
  float randf01()
  {
    return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
  }

  // Get random float number
  float randf(int rangeMax)
  {
    return static_cast<float>(rand() % rangeMax) * randf01();
  }

  // Write random square matrix with given dimensions
  void dumpRandfMatrix(std::ostream& file, int dim, int rangeMax)
  {
    for (int row = 0; row < dim; ++row)
    {
      for (int col = 0; col < dim; ++col)
      {
        float pixel = randf(rangeMax);

        file << pixel;

        if (col != dim - 1)
        {
          file << ' ';
        }
      }

      if (row != dim - 1)
      {
        file << std::endl;
      }
    }
  }
}

void InputGenerator::doJob(const std::string& fileName, int imageDim, int maskDim)
{
  static const int cImagePixelRangeMax = 255;
  static const int cMaskRangeMax       = 10;

  std::ofstream file(fileName);

  assert(file.is_open());

  file << imageDim << " " << maskDim << std::endl;

  // Write image matrix
  dumpRandfMatrix(file, imageDim, cImagePixelRangeMax); // Some dummy max value range
  file << std::endl;
  // Flush file buffer
  file.flush();
  // Write mask matrix
  dumpRandfMatrix(file, maskDim, cMaskRangeMax);

  file.close();
}