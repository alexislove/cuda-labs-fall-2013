/*!
 *\file TaskSerializable.cpp
 *\brief This file contains input/output serialization class implementation
 */

#include <algorithm>
#include <cassert>
#include <string>
#include <sstream>
#include <vector>

#include "TaskSerializable.h"

#define IMAGE_DIM_LIMIT 32768
#define MASK_DIM_LIMIT 9

namespace 
{
  bool readMatrix(std::istream& input, float* matrix, int width, int height)
  {
    using namespace std;

    // Parse following mPseudoImageHeight lines of the file
    for (int row = 0; row < height; ++row)
    {
      string currentLine;
      if (!getline(input, currentLine))
      {
        return false;
      }
      if (currentLine.empty())
      {
        --row;
        continue;
      }

      // Fill matrix row
      istringstream iss(currentLine);

      istream_iterator<string> token(iss);
      istream_iterator<string> end;

      int column = 0;
      for_each(token, end, [&matrix, &width, &row, &column](const string& s) {
        stringstream ss(s);
        ss >> matrix[row * width + column];
        ++column;
      });

      // Ill-formed input
      if (column != width) 
      {
        return false;
      }
    }

    return true;
  }
}

TaskSerializable::TaskSerializable()
  : mPseudoImage(nullptr),
    mPseudoImageWidth(0),
    mPseudoImageHeight(0),
    mConvolutionMask(nullptr),
    mConvolutionMaskWidth(0),
    mConvolutionMaskHeight(0)
{

}

TaskSerializable::~TaskSerializable()
{
  if (mPseudoImage)
  {
    delete[] mPseudoImage;
  }
  if (mConvolutionMask)
  {
    delete[] mConvolutionMask;
  }
}

bool TaskSerializable::read(std::istream& input)
{
  if (input.eof())
  {
    return false;
  }

  // Reset
  setPseudoImage(nullptr, 0, 0);
  setConvolutionMask(nullptr, 0, 0);

  using namespace std;

  // Square dimensions are given in the input file

  // Read pseudo image dimensions
  int dim = 0;
  input >> dim;

  // For convenience of assert description hard-coded constants
  assert(dim >= 0 && dim <= 32768 && "Only mentioned dimensions are supported");

  mPseudoImageWidth = mPseudoImageHeight = dim;

  // Read convolution mask dimensions
  input >> dim;

  assert(dim >= 0 && dim <= 9 && "Only mentioned dimensions are supported");

  // Force input to stop reading line
  //string dummy;
  //getline(input, dummy);

  mConvolutionMaskWidth = mConvolutionMaskHeight = dim;

  // Allocate and read image
  mPseudoImage = new float[mPseudoImageWidth * mPseudoImageHeight];
  if (!readMatrix(input, mPseudoImage, mPseudoImageWidth, mPseudoImageHeight))
  {
    return false;
  }
  // Allocate and read mask
  mConvolutionMask = new float[mConvolutionMaskWidth * mConvolutionMaskHeight];
  if (!readMatrix(input, mConvolutionMask, mConvolutionMaskWidth, mConvolutionMaskHeight))
  {
    return false;
  }

  return true;
}

bool TaskSerializable::write(std::ostream& output) const
{
  // Write only 3 signs
  //output.precision(3);

  for (int row = 0; row < mPseudoImageHeight; ++row)
  {
    for (int col = 0; col < mPseudoImageWidth; ++col)
    {
      output << mPseudoImage[row * mPseudoImageWidth + col];
      // Don't add space in the last column
      if (col != mPseudoImageWidth - 1)
      {
        output << " ";
      }
    }
    // Don't add new line in the last row
    if (row != mPseudoImageHeight - 1)
    {
      output << std::endl;
    }  
  }

  return true;
}