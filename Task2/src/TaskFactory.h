/*! 
 *\file TaskFactory.h
 *\brief This file contains extern task factory routines definition
 */

#ifndef TASKFACTORY_H
  #define TASKFACTORY_H

  struct ITask;
  
  /*! 
   *\brief Create CPU task instance
   *\return CPU task instance pointer, caller takes ownership of it
   */
  extern ITask* GCreateCPUTask();

  /*! 
   *\brief Create GPU task with global memory usage instance
   *\return GPU task instance pointer, caller takes ownership of it
   */
  extern ITask* GCreateGPUTaskGlobalMemory();

  /*! 
   *\brief Create GPU task with shared memory usage instance
   *\return GPU task instance pointer, caller takes ownership of it
   */
  extern ITask* GCreateGPUTaskSharedMemory();

#endif // TASKFACTORY_H