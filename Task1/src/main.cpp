/*!
 *\file main.cpp
 *\brief Application entry point
 */

#include "InputGenerator.h"
#include "ITask.h"
#include "TaskFactory.h"

int main(int argc, char* argv[])
{
  // Uncomment this to generate input file with desired dimensions 
  InputGenerator::doJob("input.txt", 5, 5);
   

  // Uncomment this to enable CPU solution
  ITask *cpuTask = GCreateCPUTask();
  if (!cpuTask->solve("input.txt", "outputcpu.txt"))
  {
    return -1;
  }
  

  ITask *gpuTask = GCreateGPUTask();

  if (!gpuTask->solve("input.txt", "outputgpu.txt"))
  {
    return -1;
  }

  return 0;
}