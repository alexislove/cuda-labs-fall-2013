/*!
  *\file CpuTask.cpp
  *\brief This file contains CPU task implementation
 */

#include <cuda.h>
#include <cuda_runtime.h>

#include <fstream>

#include "CpuProfiler.h"
#include "GpuProfiler.h"
#include "TaskSerializable.h"
#include "ITask.h"

#include "RGBGSKernel.cuh"

// This macro compares error code with CUDA_SUCCESS and in case smth failed - prints CUDA error message and returns false
#define CU_B_RETURN(e) if ((e) != CUDA_SUCCESS) { std::cout << cudaGetErrorString((e)) << std::endl; return false; }

struct KernelMap
{
  float3* rgb;
  float* gray;
};


class GpuTask : public ITask
{
public:
  GpuTask()
    : mDevice(NULL)
  {

  }

  virtual ~GpuTask() 
  {

  }

  virtual bool solve(const std::string& inputFileName, const std::string& outputFileName); 

private:
  bool readInput(const std::string& fileName, float** rgbMatrix, int* rows, int *cols);

  bool writeOutput(const std::string& fileName, const float *grayScale, int rows, int cols);

  bool initializeKernel(const float *rgbMatrix, int rows, int cols);

  bool obtainGrayscale(int rows, int cols, float **grayscale);

  bool releaseKernel();

private:
  KernelMap *mDevice;
};

bool GpuTask::solve(const std::string& inputFileName, const std::string& outputFileName)
{
  CpuProfiler totalProfiler;
  CpuProfiler taskProfiler;
  GpuProfiler gpuTaskProfiler;

  std::cout << "******************GPU Task***************************" << std::endl;

  gpuTaskProfiler.initialize();

  float *rgbMatrix = NULL;
  int   rows = 0, cols = 0;

  totalProfiler.start();
  if (!readInput(inputFileName, &rgbMatrix, &rows, &cols))
  {
    return false;
  }

  taskProfiler.start();
  // Initialize CUDA and copy data to device memory
  if (!initializeKernel(rgbMatrix, rows, cols))
  {
    return false;
  }

  gpuTaskProfiler.start();
  // Run kernel on the input data
  if (GRunRGB2GrayscaleBranchedKernel(mDevice->rgb, rows, cols, mDevice->gray) != cudaSuccess)
  {
    return false;
  }
  gpuTaskProfiler.stop();

  float *grayScale = NULL;
  if (!obtainGrayscale(rows, cols, &grayScale))
  {
    return false;
  }

  if (!releaseKernel())
  {
    return false;
  }
  taskProfiler.stop();

  if (!writeOutput(outputFileName, grayScale, rows, cols))
  {
    return false;
  }

  totalProfiler.stop();

  std::cout << "Total execution time:  " << totalProfiler.getMilliSeconds()   << " ms (" << totalProfiler.getNanoSeconds()            << " ns)" << std::endl;
  std::cout << "Task execution time:   " << taskProfiler.getMilliSeconds()    << " ms (" << taskProfiler.getNanoSeconds()             << " ns)" << std::endl;
  std::cout << "Kernel execution time: " << gpuTaskProfiler.getMilliseconds() << " ms (" << gpuTaskProfiler.getMilliseconds() * 10e6f << " ns)" << std::endl;

  gpuTaskProfiler.release();

  delete[] rgbMatrix;
  delete[] grayScale;

  return true;
}

bool GpuTask::readInput(const std::string& fileName, float **rgbMatrix, int *rows, int *cols)
{
  TaskSerializable ts;

  std::ifstream input(fileName);

  if (!input.is_open())
  {
    return false;
  }

  return ts.read(input, rgbMatrix, rows, cols);
}

bool GpuTask::writeOutput(const std::string& fileName, const float *grayScale, int rows, int cols)
{
  TaskSerializable ts;

  std::ofstream output(fileName);

  if (!output.is_open())
  {
    return false;
  }

  return ts.write(output, grayScale, rows, cols);
}

bool GpuTask::initializeKernel(const float *rgbMatrix, int rows, int cols)
{
  cudaError cuError = cudaSuccess;

  cuError = cudaSetDevice(0);
  CU_B_RETURN(cuError);

  mDevice = new KernelMap;

  const int rgbBytesSize  = rows * cols * sizeof(float3);
  const int grayBytesSize = rows * cols * sizeof(float);

  // Allocate memory on the device for source and target images
  cuError = cudaMalloc(&mDevice->rgb, rgbBytesSize);
  CU_B_RETURN(cuError);
  cuError = cudaMalloc(&mDevice->gray, grayBytesSize);
  CU_B_RETURN(cuError);
  cuError = cudaMemset(mDevice->gray, 0, grayBytesSize);
  CU_B_RETURN(cuError);
  cuError = cudaMemcpy(mDevice->rgb, rgbMatrix, rgbBytesSize, cudaMemcpyHostToDevice);
  CU_B_RETURN(cuError);

  return true;
}

bool GpuTask::obtainGrayscale(int rows, int cols, float **grayscale)
{
  cudaError cuError = cudaSuccess;

  const int graySize      = rows * cols;
  const int grayBytesSize = graySize * sizeof(float);
  *grayscale = new float[graySize];

  cuError = cudaMemcpy(*grayscale, mDevice->gray, grayBytesSize, cudaMemcpyDeviceToHost);
  CU_B_RETURN(cuError);

  return true;
}

bool GpuTask::releaseKernel()
{
  cudaError cuError = cudaSuccess;

  cuError = cudaFree(mDevice->rgb);
  CU_B_RETURN(cuError);
  cuError = cudaFree(mDevice->gray);
  CU_B_RETURN(cuError);

  delete mDevice;

  return true;
}

ITask* GCreateGPUTask()
{
  return new GpuTask;
}