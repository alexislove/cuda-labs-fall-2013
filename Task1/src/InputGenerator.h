/*!
 *\file InputGenerator.h
 *\brief This file contains InputGenerator class definition
 */

#ifndef INPUTGENERATOR_H
  #define INPUTGENERATOR_H

  #include <string>

  /*!
   *\class InputGenerator 
   *\brief This class represents input data generator for the specified task
   */
  struct InputGenerator
  {
    /*!
     *\brief Perform input data generation
     *\param fileName Desired file name
     *\param rows Number of rows to be written
     *\param columns Number of columns to be written
     */
    static void doJob(const std::string& fileName, int rows, int columns);

  };

#endif // INPUTGENERATOR_H