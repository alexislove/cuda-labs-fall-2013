/*!
 *\file CpuTask.cpp
 *\brief This file contains CPU task implementation
 */

#include <chrono>
#include <fstream>

#include "CpuProfiler.h"
#include "TaskSerializable.h"

#include "ITask.h"

#define RGB_2_GS(r, g, b) (r) * 0.299f + (g) * 0.587f + (b) * 0.114f

class CpuTask : public ITask
{
public:
  virtual ~CpuTask() 
  {

  }

  virtual bool solve(const std::string& inputFileName, const std::string& outputFileName); 

private:
  bool readInput(const std::string& fileName, float** rgbMatrix, int* rows, int *cols);

  bool writeOutput(const std::string& fileName, const float *grayScale, int rows, int cols);

  /*!
   *\brief Convert input RGB matrix into gray scale output one
   *\param rows Number of rows in input RGB matrix
   *\param cols Number of columns in input RGB matrix
   *\param out Output array after conversion
   */
  void rgbToGrayScale(const float* in, int rows, int cols, float **out);
};

bool CpuTask::solve(const std::string& inputFileName, const std::string& outputFileName)
{
  CpuProfiler totalProfiler;
  CpuProfiler taskProfiler;

  std::cout << "******************CPU Task***************************" << std::endl;

  float *rgbMatrix = NULL;
  int   rows = 0, cols = 0;

  totalProfiler.start();
  if (!readInput(inputFileName, &rgbMatrix, &rows, &cols))
  {
    return false;
  }

  float *grayScale = NULL;

  taskProfiler.start();
  rgbToGrayScale(rgbMatrix, rows, cols, &grayScale);
  taskProfiler.stop();
    
  if (!writeOutput(outputFileName, grayScale, rows, cols))
  {
    return false;
  }
  totalProfiler.stop();

  std::cout << "Total execution time:  " << totalProfiler.getMilliSeconds()   << " ms (" << totalProfiler.getNanoSeconds()            << " ns)" << std::endl;
  std::cout << "Task execution time:   " << taskProfiler.getMilliSeconds()    << " ms (" << taskProfiler.getNanoSeconds()             << " ns)" << std::endl;

  delete[] rgbMatrix;
  delete[] grayScale;

  return true;
}

void CpuTask::rgbToGrayScale(const float* in, int rows, int cols, float **out)
{
  *out = new float[rows * cols];
  const int elementsPerRow = 3 * cols;
  for (int row = 0; row < rows; ++row)
  {
    for (int col = 0; col < cols; ++col)
    {
      float r = in[row * elementsPerRow + 3 * col + 0];
      float g = in[row * elementsPerRow + 3 * col + 1];
      float b = in[row * elementsPerRow + 3 * col + 2];

      (*out)[row * cols + col] = RGB_2_GS(r, g, b);
    }
  }
}

bool CpuTask::readInput(const std::string& fileName, float **rgbMatrix, int *rows, int *cols)
{
  TaskSerializable ts;

  std::ifstream input(fileName);

  if (!input.is_open())
  {
    return false;
  }

  return ts.read(input, rgbMatrix, rows, cols);
}

bool CpuTask::writeOutput(const std::string& fileName, const float *grayScale, int rows, int cols)
{
  TaskSerializable ts;

  std::ofstream output(fileName);

  if (!output.is_open())
  {
    return false;
  }

  return ts.write(output, grayScale, rows, cols);
}

ITask* GCreateCPUTask()
{
  return new CpuTask;
}

