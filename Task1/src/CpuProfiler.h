/*!
 *\file CpuProfiler.h
 *\brief This file contains CPU profiler
 */

#ifndef CPUPROFILER_H
  #define CPUPROFILER_H

  #include <chrono>
  #include <string>

  /*!
   *\class CpuProfiler
   *\brief This class represents CPU-related profiler
   */
  class CpuProfiler
  {
    typedef std::chrono::milliseconds            Milliseconds;
    typedef std::chrono::microseconds            Microseconds;
    typedef std::chrono::nanoseconds             Nanoseconds;
    typedef std::chrono::system_clock            SystemClock;
    typedef std::chrono::time_point<SystemClock> TimePoint;
  public:

    /*!
     *\brief Constructor
     */
    CpuProfiler()
    {

    }

    /*!
     *\brief Reset time points
     */
    void reset()
    {
      mStart = TimePoint();
      mEnd   = TimePoint();
    }

    /*!
     *\brief Start profiling
     */
    void start()
    {
      mStart = SystemClock::now();
    }

    /*!
     *\brief End profiling
     */
    void stop()
    {
      mEnd = SystemClock::now();
    }

    /*!
     *\brief Get nanoseconds time
     *\return Execution time in nanoseconds
     */
    long long getNanoSeconds()
    {
      return std::chrono::duration_cast<Nanoseconds>(mEnd - mStart).count();
    }

    /*!
     *\brief Get microseconds time
     *\return Execution time in nanoseconds
     */
    long long getMicroSeconds()
    {
      return std::chrono::duration_cast<Microseconds>(mEnd - mStart).count();
    }

    /*!
     *\brief Get milliseconds time
     *\return Execution time in nanoseconds
     */
    long long getMilliSeconds()
    {
      return std::chrono::duration_cast<Milliseconds>(mEnd - mStart).count();
    }

  private:
    //! Profiling start time
    TimePoint mStart;
    //! Profiling end time
    TimePoint mEnd;
  };
  
#endif // CPUPROFILER_H