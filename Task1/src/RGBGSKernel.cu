/*!
 *\file Kernel.cu
 *\brief This file contains CUDA kernel implementation
 */

#include <cmath>

#include "CudaRuntimeWrapper.h"

#include "RGBGSKernel.cuh"

#define RGB_2_GS(r, g, b) (r) * 0.299f + (g) * 0.587f + (b) * 0.114f

__global__ void cuRgbToGrayScaleBranched(const float3* rgb, int rows, int cols, float* const gray)
{
  const int index = blockIdx.x * blockDim.x + threadIdx.x;

  if (index < rows * cols)
  {
    float3 src  = rgb[index];
    gray[index] = RGB_2_GS(src.x, src.y, src.z);
  }  
}

cudaError GRunRGB2GrayscaleBranchedKernel(const float3 *rgbMatrix, int rows, int cols, float* gray)
{
  cudaError cuError = cudaSuccess;

  int deviceId = 0;
  if (!CudaRuntimeWrapper::getBestCudaDeviceId(&deviceId))
  {
    return cudaErrorDevicesUnavailable;
  }

  CudaRuntimeWrapper wrapper(deviceId);

  int blockSize = 0;
  int gridSize  = 0;

  wrapper.getOptBlockAndGridSize(rows * cols, &blockSize, &gridSize);

  cuRgbToGrayScaleBranched<<<gridSize, blockSize>>>(rgbMatrix, rows, cols, gray);

  cuError = cudaDeviceSynchronize();

  return cuError;
}