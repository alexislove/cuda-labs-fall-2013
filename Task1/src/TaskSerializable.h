/*!
 *\file TaskSerializable.h
 *\brief This file contains input/output serialization class definition
 */

#ifndef TASKSERIALIZABLE_H
  #define TASKSERIALIZABLE_H

  #include <iostream>
  
  /*!
   *\class TaskSerializable
   *\brief This class represents input/output data serialization object
   */
  struct TaskSerializable
  {
    /*!
     *\brief Read data from the input stream to the output array
     *      Input data contains (r g b) triples, so that output array will contain 3 * K elements,
     *      where K = rows * cols - number of elements in the input matrix
     *\param input Input stream
     *\param out   Output data array (not initialized)
     *\param rows     Number of rows in RGB matrix
     *\param cols     Number of columns in RGB matrix
     *\return True, in case reading succeeded
     */
    bool read(std::istream& input, float **out, int *rows, int *cols);

    /*!
     *\brief Write data into the output stream - cols elements per rows rows
     *\param output Output stream
     *\param in     Input data to write
     *\param rows      Number of rows to write
     *\param cols      Number of columns to write
     *\return True, in case writing succeded
     */
    bool write(std::ostream& output, const float *in, int rows, int cols);

  };

  

#endif // TASKSERIALIZABLE_H