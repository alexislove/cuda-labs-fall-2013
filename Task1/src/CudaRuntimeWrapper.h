/*!
 *\file CudaRuntimeWrapper.h
 *\brief This file contains CudaRuntimeWrapper helper class declaration
 */

#ifndef CUDARUNTIMEORACLE_H
  #define CUDARUNTIMEORACLE_H

  #include <iostream>
  
  /*!
   *\class CudaRuntimeWrapper
   *\brief This is a helper class to obtain CUDA device info and to select optimal
   *      grid/block dimensions
   */
  class CudaRuntimeWrapper
  {
  public:
    //! Use independent type for CUDA device id
    typedef int CudaDeviceId;

    /*!
     *\brief Get CUDA devices count
     *\param outCount Actual number of devices, that supports CUDA
     *\return True, in case at least one CUDA device is available
     */
    static bool getCudaDeviceCount(int* outCount);

    /*!
     *\brief Get best CUDA device id
     *\param outDeviceId Id of the most powerful device
     *\return True, in case id was successfully retrieved
     */
    static bool getBestCudaDeviceId(int *outDeviceId);

  public:
    /*!
     *\brief Constructor
     *\param deviceId CUDA device identifier
     */
    CudaRuntimeWrapper(CudaDeviceId deviceId);

    /*!
     *\brief Dump CUDA device info to the given stream
     *\param stream Output stream, defaults to cout
     */
    void dumpCudaDeviceInfo(std::ostream& stream = std::cout) const;

    /*!
     *\brief Calculate optimal grid and block size for the given device
     *\param inputSize Input data size
     *\param outBlockSize Calculated block size
     *\param outGridSize  Calculated grid size
     *\return True, in case no CUDA errors were met during the calculation
     */
    bool getOptBlockAndGridSize(int inputSize, int* outBlockSize, int* outGridSize) const;

  private:
    //! Current device id
    CudaDeviceId mDeviceId;
  };

#endif // CUDASTATS_H