/*!
 *\file RGBGSKernel.cuh
 *\brief This file contains host routine to run CUDA kernel
 */

#ifndef RGBGSKERNEL_CUH
  #define RGBGSKERNEL_CUH

  #include <cuda_runtime.h>

  /*!
   *\brief This routines starts rgb 2 grayscale CUDA conversion kernel with branching
   *\param rgbMatrix Source RGB matrix
   *\param rows Number of rows in the matrix
   *\param cols Number of columns in the matrix
   *\param gray Output gray scale array
   *\return CUDA error code
   */
  cudaError GRunRGB2GrayscaleBranchedKernel(const float3 *rgbMatrix, int rows, int cols, float* gray);
  

#endif // RGBGSKERNEL_CUH