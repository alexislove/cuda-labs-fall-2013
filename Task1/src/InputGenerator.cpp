/*!
 *\file InputGenerator.cpp
 *\brief This file contains InputGenerator class definition
 */

#include <cassert>
#include <cstdlib>
#include <fstream>

#include "InputGenerator.h"

namespace
{
  // Get random number from 0.0 to 1.0 inclusive
  float randf()
  {
    return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
  }
}

void InputGenerator::doJob(const std::string& fileName, int rows, int columns)
{
  std::ofstream file(fileName);

  assert(file.is_open());

  file << rows << " " << columns << std::endl;

  for (int row = 0; row < rows; ++row)
  {
    for (int col = 0; col < columns; ++col)
    {
      float r = randf();
      float g = randf();
      float b = randf();

      file << "(" << r << " " << g << " " << b << ")";

      if (col != columns - 1)
      {
        file << " ";
      }
    }

    if (row != rows - 1)
    {
      file << std::endl;
    }
  }
}