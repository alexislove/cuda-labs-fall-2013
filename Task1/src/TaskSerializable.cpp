/*!
 *\file TaskSerializable.cpp
 *\brief This file contains input/output serialization class implementation
 */

#include <algorithm>
#include <cassert>
#include <string>
#include <sstream>
#include <vector>

#include "TaskSerializable.h"

bool TaskSerializable::read(std::istream& input, float **out, int *rows, int *cols)
{
  using namespace std;

  int rowsCount = 0;
  int colsCount = 0;
  // Read dimensions
  input >> rowsCount;
  input >> colsCount;

  *rows = rowsCount;
  *cols = colsCount;

  const int elementsPerRow = 3 * colsCount;

  // Allocate array
  *out = new float[rowsCount * elementsPerRow];


  // Parse the rest of the file
  int row = 0;
  string currentLine;
  vector<string> tokens(elementsPerRow);  
  while (getline(input, currentLine))
  {
    // Skip empty strings
    if (currentLine.empty())
    {
      continue;
    }

    if (row > rowsCount)
    {
      return false;
    }
    
    istringstream iss(currentLine);

    // Split everything by the space
    // *** Too slow ***
    //copy(istream_iterator<string>(iss),
    //    istream_iterator<string>(),
    //     back_inserter(tokens));
    istream_iterator<string> token(iss);
    istream_iterator<string> end;

    for (int idx = 0; idx < elementsPerRow; ++idx, ++token)
    {
      assert(token != end);
      tokens[idx] = *token;
    }

    // Iterate over tokens, removing ( from each 0s and ) from each 2nds
    int component = 0;
    for (int idx = 0; idx < elementsPerRow; ++idx)
    {
      std::string& token = tokens[idx];

      // Trick: first compare to each 0s (3rd) element, then to each 2nd
      if (component == 0)
      {
        // Too slow
        //const size_t bracketPos = token.find_first_of('(');
        //assert(bracketPos != string::npos);
        const size_t bracketPos = 0;
        token.erase(bracketPos, 1);
      }
      else if (component == 2)
      {
        // Too slow
        //const size_t bracketPos = token.find_first_of(')');
        //assert(bracketPos != string::npos);
        const size_t bracketPos = token.length() - 1;
        token.erase(bracketPos, 1);
      }

      stringstream ss(token);
      ss >> (*out)[row * elementsPerRow + idx];
      ++component;
      if (component > 2)
      {
        component = 0;
      }
    }

    ++row;
  }

  return true;
}

bool TaskSerializable::write(std::ostream& output, const float *in, int rows, int cols)
{
  // Write only 3 signs
  output.precision(3);
  for (int row = 0; row < rows; ++row)
  {
    for (int col = 0; col < cols; ++col)
    {
      output << in[row * cols + col];
      // Don't add space in the last column
      if (col != cols - 1)
      {
        output << " ";
      }
    }
    // Don't add new line in the last row
    if (row != rows - 1)
    {
      output << std::endl;
    }
  }

  return true;
}